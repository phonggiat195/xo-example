import React, { Component } from 'react'
import Square from './Square';

export default class Board extends Component {

    renderSquare(i){
        const {squares, winner} = this.props;
        return <Square value={squares[i]}
          onClick={() => this.props.onClick(i)} winner={winner && winner.includes(i) ? 'winner' : ''} />
      };
    // renderAllSquares(){
    //     const matrixSize = Math.sqrt(this.props.squares.length);
    //     const board = Array(matrixSize).fill(null);
    //     for(let i = 0; i < matrixSize; i++){
    //         const squares = Array(matrixSize).fill(null);
    //         for(let j = 0; j < matrixSize; j++){
    //             var squareKey = i * matrixSize + j;
    //             squares.push(<span key={squareKey}>{this.renderSquare(squareKey)}</span>);
    //         }
    //         board.push(<div key={i}>{squares}</div>);
    //     }
    //     return board;
    //   }
    render() {

        const rowsWidth = Array(Math.sqrt(this.props.squares.length)).fill(null);
        const celsWidth = rowsWidth;
        const board = rowsWidth.map((row, i) => {
      const squares = celsWidth.map((cel, j) => {
        const squareIndex = i * rowsWidth.length + j;
        return(
          <span key={squareIndex}>{this.renderSquare(squareIndex)}</span>
        );
      });
      return <div key={i}>{squares}</div>
    });
        return (
            <div>
                <div>Board</div>
                <div>{board}</div>
            </div>
        )
    }
}
