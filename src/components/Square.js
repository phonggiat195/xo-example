import React, { Component } from 'react'

export default class Square extends Component {
    render() {
        const squareClass = `square ${this.props.winner}`;
        return (
                <button className={squareClass} onClick={this.props.onClick}>{this.props.value}
                </button>
            
        )
    }
}
