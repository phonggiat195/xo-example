import React, { Component } from 'react'
import Board from './Board';


function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
          return { // Chúng ta sẽ gửi về 2 giá trị là: Người thắng cuộc và vị trí thắng cuộc.
            winnerLocation: [a,b,c],
            winnerPlayer: squares[a]
          };
        }
      }
      return null;
    }

export default class Game extends Component {
    constructor(){
        super();
        this.state = {
            history: [{
          squares: Array(9).fill(null),
          moveLocation: '',
        }],
          xIsNext: true,
          stepNumber: 0,
          isReverse: false, // Thêm cờ isReverse để nhận biết hướng sắp xếp
        };
      }

    handleClick(i){
        console.log(i);
        const history = this.state.history.slice(0, this.state.stepNumber + 1); 
        // Chúng ta cần clone history ra bản phụ tránh làm ảnh hưởng bản chính
        const current = history[history.length - 1];
        // Lấy history của lần gần nhất
        const squares = current.squares.slice();
    if(calculateWinner(squares) || squares[i]){
      return;
    }
    const matrixSize = Math.sqrt(history[0].squares.length);
    const moveLocation = [Math.floor(i / matrixSize) + 1, (i % matrixSize) + 1].join(", "); 
    // (row = vị trí / matrixSize, col = vị trí % matrixSize)
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
        history: history.concat([{ 
            // Thêm history mỗi khi click vào ô vuông
          squares,
          moveLocation,
        }]),
        xIsNext: !this.state.xIsNext,
        stepNumber: history.length
         // Vì stepNumber bằng với độ dài của history.
      });
  }

  jumpTo(move){
    this.setState({
      xIsNext: (move % 2) ? false : true,
      stepNumber: move,
    })
  }

  changeReverse(isReverse){
    this.setState({
      isReverse: !isReverse,
    })
  }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber]; 
        const squares = current.squares;
        const winner = calculateWinner(squares);
        const isReverse = this.state.isReverse;
        let status;
            if(winner){
            status = "Winner is: " + winner.winnerPlayer; 
            // Nếu winner có giá trị thì sẽ hiển thị người thắng cuộc
            }else if(this.state.stepNumber === 9){ 
                // Nếu sau 9 lần chưa có ai win thì hòa
            status = "No one win"; 
            }else{
            status = "Next player is: " + (this.state.xIsNext ? 'X' : 'O');
            }

            
        const moves = history.map((step, move) => {
        const description = move ? `Move #${move} (${step.moveLocation})` : 'Game start'; 
        // move = 0 là lúc game mới start.
        return <li key={move}><a href="#" onClick={() => this.jumpTo(move)}>{description}</a></li> 
        // Thêm thẻ a và disable href đi, thêm sự kiện onClick vào
      });

      

        return (
            <div>
            <div className="game">
              <Board squares={squares} onClick={i => this.handleClick(i)} winner={winner && winner.winnerLocation}/> 
              {/* Sử dụng && để phòng T.Hợp winner là null */}
            </div>
            <div className="game-info">
              <p>{status}</p>
              <ol reversed={isReverse ? 'reverse' : ''}>{isReverse ? moves.reverse() : moves}</ol>
          <button onClick={() => this.changeReverse(isReverse)}>Reverse list</button>
            </div>
          </div>
        )
    }
}


